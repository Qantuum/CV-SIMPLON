<!DOCTYPE html>
<head>
    <title>Mon CV</title>
    <link href="https://framagit.org/Qantuum/CV-SIMPLON/blob/master/styles.css" type="text/css" rel="stylesheet">
</head>
<body>
    <h1>Bienvenue!</h1>
    <p class="presentation">Je me présente, Marvin, 25 ans, aspirant développeur web. J'apprends à coder sur codecademy et sololearning, comme recommandé sur la procédure de candidature Simplon. J'y ai acquis des badges en HTML, CSS, Javascript et PHP.</p>
</body>

<table>
<tr><th>Dates</th>
<th>Formation</th></tr>
<tr><td>2009</td><td>Obtention du BAC Scientifique option "Sciences de l'Ingénieur" au lycée Jules Ferry, Cannes</td></tr>
<tr><td>2015</td><td>Obtention d'un Master "Formulation et Evaluation dans l'Industrie des Arômes" à l'ISIPCA, Versailles</td></tr>
<tr><th>Dates</th><th>Dernière expérience professionnelle</th></tr>
<tr><td>Oct. 2015 - Oct. 2017</td><td><strong>Aromaticien Junior</strong> chez <strong>Foodarom Germany GmbH</strong> <br />Formulation d'arômes, évaluation d'applications : protéines, boissons. Tâches requiérant une gestion de projet, de l'organisation et de la créativité.</td></tr>
</table>